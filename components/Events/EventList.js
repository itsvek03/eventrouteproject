import React from 'react'
import EventItem from './EventItem'

export default function EventList(props) {
    console.log("PROPS OF EVents List",props)
    const {items} =props
    return (
        <div>
            {items.map((a) =>(
                <EventItem 
                    key={a.id}
                    id={a.id}
                    title={a.title}
                    location={a.location}
                    date={a.date}
                    image={a.image} 
                />
            ))}
        </div>
    )
}
