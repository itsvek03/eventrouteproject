import React from "react";
import Button from '../../ui/Button'

export default function EventItem(props) {
    console.log("PROPS OF EVENT ITEMS",props)
    const {title,image,date,location,id}=props

    const humanReadableDate =new Date(date).toLocaleDateString('en-US',{
        day:'numeric',
        month:'long',
        year:'numeric'
    })

    const FormattedAddress =location.replace(', ','\n')
    const exploreLink =`/events/${id}`;
  return (
    <li>
      <img src={image} alt={title} width={100}></img>
      <div>
          <div>
              <h2>{title}</h2>
          </div>

          <div>
              <h2>{FormattedAddress}</h2>
          </div>

          <div>
              <h2>{humanReadableDate}</h2>
          </div>
          <div>
              <Button link={exploreLink}>Explore Items</Button>
            
          </div>
      </div>
    </li>
  );
}
