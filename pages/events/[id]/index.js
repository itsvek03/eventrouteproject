import React from "react";
import { useRouter } from "next/router";
import { gtrFetauredEventById } from "../../../dummydata";

export default function EventBasedId() {
  const router = useRouter();

  const id = router.query.id;
  console.log(id);
  const event = gtrFetauredEventById(id);

  console.log("EVENT", event);
  if (!event) {
    return <h1>No Events Found</h1>;
  }
  return (
    <div>
      <h1>Events Based On ID</h1>
      <br />
      <ul>
        <li>{event.location}</li>
        <li>{event.id}</li>
        <li>{event.title}</li>
        <li>{event.description}</li>
        <li>{event.date}</li>
        
        <li>{event.isFeatured.toString()}</li>
        <img src={event.image} alt={event.title} width="100" />
      </ul>

      <div></div>
    </div>
  );
}
