import React from 'react'
import {getAllEvent} from '../../dummydata'

export default function Events() {
    const data = getAllEvent()
    console.log("DATA",data)
    return (
        <div>
            <h1>All Events Page</h1>
            {
                data.map((e) =>{
                    return(
                       <ul key={e.id}>
                           <li>{e.isFeatured.toString()}</li>
                           <li>{e.title}</li>
                           <li>{e.description}</li>
                           <li>{e.location}</li>
                           <li>{e.date}</li>
                           
                           <img src={e.image} alt={e.title} width="100"/>
                       </ul>
                       
                    )
                })
            }
        </div>
    )
}
