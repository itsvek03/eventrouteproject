import { getFeaturedEvents } from "../dummydata";
import EventList from "../components/Events/EventList";
/**
 *    Project Routes
 *  a) / --> Starting Page
 *  b) /events --> All Events
 *  c) /events/[id] --> Events based on id
 * d) /events/[...slug] --> Filters based on slug
 * **/

export default function Home() {
  const featuredEvents = getFeaturedEvents();
  return (
    <div>
     <EventList items={featuredEvents}/>
    </div>
  );
}
